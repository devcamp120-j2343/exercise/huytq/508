//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const courseSchema = new Schema({
    id : mongoose.Types.ObjectId,
    title : {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: false  
    },
    noStudent : {
        type: Number,
        default: 0
    },
    reviews : [{
        type: mongoose.Types.ObjectId,
        ref:"review"
    }]
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("course",courseSchema)