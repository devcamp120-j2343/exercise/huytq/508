//Import thư viện ExpressJS
// import express from 'express';
const express = require('express');
const path = require('path');
const cors = require('cors');
//import thư viện mongoose
const mongoose = require('mongoose');
//khai bao schema từ mongoose
const Schema = mongoose.Schema;

const courseRouter = require("./app/routes/course.router");
const reviewRouter = require("./app/routes/review.router");
const drinkRouter = require("./app/routes/drink.router");
const voucherRouter = require('./app/routes/voucher.router');
const userRouter = require('./app/routes/user.router');
const orderRouter = require('./app/routes/order.router');
// Khởi tạo app nodejs bằng express
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;
//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));
   
// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());
app.use(cors());
app.get("/samplePizza", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/sample.06restAPI.order.pizza365.v2022Nov.html"));

})
app.get("/sampleCourse365", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/sample.restAPI.crud.course365_v1.1.html"));

})
//khai báo các model
const reviewModel = require('./app/models/review.model');
const courseModel = require('./app/models/course.model');
const drinkModel = require('./app/models/drink.model');
const voucherModel = require('./app/models/voucher.model');
const userModel = require('./app/models/user.model');
const orderModel = require('./app/models/order.model');
//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
    var today = new Date();

    console.log("Current time: ", today);

    next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((request, response, next) => {
    console.log("Method: ", request.method);

    next();
});

// Khai báo API trả về chuỗi
app.get("/", (request, response) => {
    //Viết code xử lý trong đây
    var today = new Date();

    var resultString = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;

    response.json({
        result: resultString
    });
});

app.use("/api/v1/courses", courseRouter);
app.use("/api/v1/reviews", reviewRouter);
app.use("/api/v1/drinks",drinkRouter);
app.use("/api/v1/vouchers",voucherRouter);
app.use("/api/v1/users",userRouter);
app.use("/devcamp-pizza365/orders",orderRouter);
// Chạy ứng dụng
app.listen(port, () => {
    console.log("Ứng dụng chạy trên cổng: ", port);
})
