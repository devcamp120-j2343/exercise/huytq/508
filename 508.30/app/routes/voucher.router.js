const express = require("express");


const voucherController = require("../controllers/voucher.controller");
const voucherMiddleware = require("../middlewares/voucher.middlewares");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL voucher: ", req.url);

    next();
});

router.get("/",voucherMiddleware.getAllVoucherMiddleware,voucherController.getAllVouchers)

router.post("/", voucherMiddleware.createVoucherMiddleware ,voucherController.createVoucher);

router.get("/:voucherId",voucherMiddleware.getDetailVoucherMiddleware , voucherController.getVoucherById);

router.put("/:voucherId", voucherMiddleware.updateVoucherMiddleware ,voucherController.updateVoucher);

router.delete("/:voucherId",voucherMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucher);

module.exports = router;
