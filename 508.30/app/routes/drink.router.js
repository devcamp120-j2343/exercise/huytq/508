const express = require("express");


const drinkController = require("../controllers/drink.controller");
const drinkMiddleware = require("../middlewares/drink.middlewares");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL drink: ", req.url);

    next();
});

router.get("/",drinkMiddleware.getAllDrinkMiddleware,drinkController.getAllDrinks)

router.post("/", drinkMiddleware.createDrinkMiddleware ,drinkController.createDrink);

router.get("/:drinkId",drinkMiddleware.getDetailDrinkMiddleware ,  drinkController.getDrinkById);

router.put("/:drinkId", drinkMiddleware.updateDrinkMiddleware ,drinkController.updateDrink);

router.delete("/:drinkId",drinkMiddleware.deleteDrinkMiddleware,  drinkController.deleteDrink);

module.exports = router;
