const express = require("express");

const reviewMiddleware = require("../middlewares/review.middlewares");
const reviewController = require("../controllers/reviewController");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL reiview: ", req.url);

    next();
});

router.get("/", reviewMiddleware.getAllReviewMiddleware, reviewController.getAllReviews)

router.post("/", reviewMiddleware.createReviewMiddleware, reviewController.createReview);

router.get("/:reviewId", reviewMiddleware.getDetailReviewMiddleware, reviewController.getReviewById);

router.put("/:reviewId", reviewMiddleware.updateReviewMiddleware, reviewController.updateReview);

router.delete("/:reviewId", reviewMiddleware.deleteReviewMiddleware, reviewController.deleteReview);


module.exports = router;
