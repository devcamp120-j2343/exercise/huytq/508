const express = require("express");


const orderController = require("../controllers/orderController");
const orderMiddleware = require("../middlewares/order.middlewares");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL Order: ", req.url);

    next();
});

router.get("/",orderMiddleware.getAllOrderMiddleware,orderController.getAllOrders)

router.post("/order", orderMiddleware.createOrderMiddleware ,orderController.createOrder);

router.get("/:orderId",orderMiddleware.getDetailOrderMiddleware ,  orderController.getOrderById);

router.put("/:orderId", orderMiddleware.updateOrderMiddleware ,orderController.updateOrder);

router.delete("/:orderId",orderMiddleware.deleteOrderMiddleware,  orderController.deleteOrder);


module.exports = router;
