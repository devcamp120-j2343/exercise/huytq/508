const express = require("express");


const userController = require("../controllers/userController");
const userMiddleware = require("../middlewares/user.middlewares");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL user: ", req.url);

    next();
});

router.get("/",userMiddleware.getAllUserMiddleware,userController.getAllUsers)

router.post("/", userMiddleware.createUserMiddleware ,userController.createUser);

router.get("/:userId",userMiddleware.getDetailUserMiddleware ,  userController.getUserById);

router.put("/:userId", userMiddleware.updateUserMiddleware ,userController.updateUser);

router.delete("/:userId",userMiddleware.deleteUserMiddleware,  userController.deleteUser);

router.get("/limit-users", userController.getAllLimitUser);

router.get("/skip-users", userController.getAllSkipUser);

router.get("/sort-users", userController.getAllSortUser);

router.get("/skip-limit-users", userController.getAllSkipLimitUser);

router.get("/sort-skip-limit-users", userController.getAllSortSkipLimitUser);
module.exports = router;
