//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    starts: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        require: false  
    }
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("review",reviewSchema)