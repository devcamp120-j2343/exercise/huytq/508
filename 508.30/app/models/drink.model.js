//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const drinkSchema = new Schema({
    id: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        require: true,
        unique:true
    },
    tenNuocUong: {
        type: String,
        require: true  
    },
    donGia: {
        type: Number,
        require: true
    }
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("drink",drinkSchema)