//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const voucherSchema = new Schema({
    id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        require: true,
        
    },
    ghiChu: {
        type: String,
        require: true  
    },
    phanTramGiamGia: {
        type: Number,
        require: true
    }
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("voucher",voucherSchema)