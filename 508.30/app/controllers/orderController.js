//import thư viện mongoose
const mongoose = require('mongoose');

// import order model
const orderModel = require('../models/order.model');

// import user model
const userModel = require('../models/user.model');

//import drink model
// const create order 
const createOrder = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        userId,
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status,
    } = req.body;

    var email = req.query.email;
    console.log(email);
    const userExist = await userModel.findOne({
        email: email,
    }).exec();
    console.log(userExist);
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "userId is not valid"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if(!pizzaSize){
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizza style is require"
        })
    }
    if(!pizzaType){
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizza type is require"
        })
    }
    if(!status){
        return res.status(400).json({
            "status": "Bad Request",
            "message": "status is require"
        })
    }
    // B3: Thao tác với CSDL
    if(userExist!=null){
        var newOrder = {
            _id: new mongoose.Types.ObjectId(),
            userId,
            orderCode,
            pizzaSize,
            pizzaType,
            voucher: new mongoose.Types.ObjectId(voucher),
            drink: new mongoose.Types.ObjectId(drink),
            status,
        }
    
        try {
            const createdOrder = await orderModel.create(newOrder);
    
            const updatedUser = await userModel.findByIdAndUpdate(userId, {
                $push: { orders: createdOrder._id }
            })
    
            return res.status(201).json({
                status: "Create order successfully",
                user: updatedUser,
                data: createdOrder
            })
        } catch (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
    } else {
        return res.status(400).json({
            status: "Bad request",
            message: "Not found any user!"
        })
    }
}

const getAllOrders = async (req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.query.userId;
    //B2: kiểm tra
    if (userId !== undefined && !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "User ID is not valid"
        })
    }

    //B3: thực thi model
    try {
        if(userId === undefined) {
            const orderList = await orderModel.find();

            if(orderList && orderList.length > 0) {
                return res.status(200).json({
                    status: "Get all order sucessfully",
                    data: orderList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any order"
                })
            }
        } else {
            const userInfo = await userModel.findById(userId).populate("orders");

            return res.status(200).json({
                status: "Get all order of customer sucessfully",
                data: userInfo.orders
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getOrderById = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const orderInfo = await orderModel.findById(orderId);

        if (orderInfo) {
            return res.status(200).json({
                status:"Get order by id sucessfully",
                data: orderInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    const {pizzaSize,pizzaType,status} = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    if(!pizzaSize){
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizza style is require"
        })
    }
    if(!pizzaType){
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizza type is require"
        })
    }
    if(!status){
        return res.status(400).json({
            "status": "Bad Request",
            "message": "status is require"
        })
    }

    //B3: thực thi model
    try {
        let updateOrder = {
            pizzaSize,pizzaType,status
        }

        const updatedOrder = await orderModel.findByIdAndUpdate(orderId, updateOrder);

        if (updatedOrder) {
            return res.status(200).json({
                status:"Update order sucessfully",
                data: updatedOrder
            })
        } else {
            return res.status(404).json({
                status:"Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteOrder = async (req, res) => {
    //B1: Thu thập dữ liệu
    var orderId = req.params.orderId;
    // Nếu muốn xóa id thừa trong mảng order thì có thể truyền thêm userId để xóa
    var userId = req.query.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedOrder = await orderModel.findByIdAndDelete(orderId);

        // Nếu có userId thì xóa thêm (optional)
        if(userId !== undefined) {
            await userModel.findByIdAndUpdate(userId, {
                $pull: { orders: orderId }
            })
        }

        if (deletedOrder) {
            return res.status(200).json({
                status:"Delete order sucessfully",
                data: deletedOrder
            })
        } else {
            return res.status(404).json({
                status:"Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrder,
    deleteOrder
}


