//import thư viện mongoose
const mongoose = require('mongoose');
//import voucher model
const voucherModel = require('../models/voucher.model');

const createVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    const {maVoucher, ghiChu, phanTramGiamGia} = req.body;
    //B2: kiểm tra dữ liệu
    if (!maVoucher) {
        return res.status(400).json({
            status:"Bad request",
            message:"ma voucher is required!"
        })
    }
    if (!ghiChu) {
        return res.status(400).json({
            status:"Bad request",
            message:"ghi chú is required!"
        })
    }
    if(phanTramGiamGia<0){
        return res.status(400).json({
            status:"Bad request",
            message:"% Giam gia is invalid"
        })
    }

    //B3: thực hiện thao tác model
    let newVoucher = {
        maVoucher,
        ghiChu,
        phanTramGiamGia
    }

    voucherModel.create(newVoucher)
                .then((data) => {
                    return res.status(201).json({
                        status:"Create new voucher sucessfully",
                        data
                    })
                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

const getAllVouchers = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    voucherModel.find()
                .then((data) => {
                    if (data && data.length > 0) {
                        return res.status(200).json({
                            status:"Get all voucher sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any voucher",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

const getVoucherById = (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    voucherModel.findById(voucherId)
                .then((data) => {
                    if (data) {
                        return res.status(200).json({
                            status:"Get voucher by id sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any voucher",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

const updateVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;
    const {maVoucher, ghiChu, phanTramGiamGia} = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    if (!maVoucher) {
        return res.status(400).json({
            status:"Bad request",
            message:"ma voucher is required!"
        })
    }
    if (!ghiChu) {
        return res.status(400).json({
            status:"Bad request",
            message:"ghi chú is required!"
        })
    }
    if(phanTramGiamGia<0){
        return res.status(400).json({
            status:"Bad request",
            message:"% Giam gia is invalid"
        })
    }
    //B3: thực thi model
    let updateVoucher = {
        maVoucher,
        ghiChu,
        phanTramGiamGia
    }

    voucherModel.findByIdAndUpdate(voucherId, updateVoucher)
                .then((data) => {
                    if (data) {
                        return res.status(200).json({
                            status:"Update voucher sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any voucher",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
    
}

const deleteVoucher = (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    voucherModel.findByIdAndDelete(voucherId)
                .then((data) => {
                    if (data) {
                        return res.status(200).json({
                            status:"Delete voucher sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any voucher",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

module.exports = { getAllVouchers, createVoucher, getVoucherById, updateVoucher, deleteVoucher }
