const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
//import thư viện mongoose
const mongoose = require("mongoose");
// Khởi tạo Express App
const app = express();
//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_COURSE365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));
//khai báo các model
const courseModel = require('./app/models/course365.model');
//khai bao port
const port = 8000;
//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/views"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/index.html"));
})
app.get("/admin",(request, response) => {
  console.log(__dirname);
  response.sendFile(path.join(__dirname + "/views/admin/admin.html"));
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
