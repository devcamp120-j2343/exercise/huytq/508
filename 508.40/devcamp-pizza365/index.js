const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

//import thư viện mongoose
const mongoose = require('mongoose');

// Khởi tạo Express App
const app = express();

const port = 8000;

//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));

//khai báo các model
const drinkModel = require('./app/models/drink.model');
const voucherModel = require('./app/models/voucher.model');
const orderModel = require('./app/models/order.model');
const userModel = require('./app/models/user.model');
//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/views"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/pizza365index.html"));
})



app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
