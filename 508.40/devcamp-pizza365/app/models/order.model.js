//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const orderSchema = new Schema({
    id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        require: true
    },
    pizzaSize: {
        type: String,
        require: true  
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: [{
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    }],
    drink :[{
        type: mongoose.Types.ObjectId,
        ref: "drink"
    }],
    status : {
        type: String,
        require:true
    }
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("order",orderSchema)