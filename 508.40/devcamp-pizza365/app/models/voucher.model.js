//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const voucherSchema = new Schema({
    id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        require: true,
        unique: true
    },
    phanTramGiamGia: {
        type: Number,
        require: true  
    },
    ghiChu: {
        type: String,
        require: false
    }
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("voucher",voucherSchema)