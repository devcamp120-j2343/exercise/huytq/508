//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const userSchema = new Schema({
    id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true  
    },
    address: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require:true
    },
    orders :[{
        type: mongoose.Types.ObjectId,
        ref: "order"
    }],
    
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("user",userSchema)